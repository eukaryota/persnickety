# persnickety license v1.0

the persnickety license is a simple copyleft license. it is almost identical to the ISC license, however it has a few additional requirements- it requires a project to provide access to source code and for redistributions of the project to have the same permissions, inspired by the GPLv3, and have the same list of conditions, inspired by the BSD-2 license.
